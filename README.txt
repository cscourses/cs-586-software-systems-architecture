All the following operations must be run from the main SSA-Project directory.

To Compile:
	javac -d ./compiled @sources.txt
	javac -d ./compiled -cp ./compiled Main.java

To Run with GasPump_1:
	java -cp ./compiled Main 1

To Run with GasPump_2:
	java -cp ./compiled Main 2