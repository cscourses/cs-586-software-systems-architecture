
/*
Main.java

This file is used to run tests on either GasPump_1 or GasPump_2 implementations.
*/

import java.util.Scanner;
import compiled.*;

public class Main {
    public static State [] getStateList(DataStore d, AbstractFactory af){
        // Create operations object
        OP op = new OP(d, af);

        // Create list of states
        Start start = new Start(op);
        S0 s0 = new S0(op);
        S1 s1 = new S1(op);
        S2 s2 = new S2(op);
        S3 s3 = new S3(op);
        S4 s4 = new S4(op);
        S5 s5 = new S5(op);
        S6 s6 = new S6(op);
        S7 s7 = new S7(op);
        State [] ls = {start, s0, s1, s2, s3, s4, s5, s6, s7};

        return ls;
    }
    public static void main(String[] args) {

        // Start test drivers
        if (args.length != 1) {
            System.out.println("Usage: Must have exactly one argument. You may enter\n  1: To select gas pump 1\n  2: To select gas pump 2\n\n");
        }
        else { // If we DO have exactly 1 argument, check argument
            switch (args[0]) {
                
                // Run Test for Gas Pump 1
                case "1": { 
                    DS1 ds1 = new DS1();
                    CF1 cf1 = new CF1(ds1);
                    State [] ls = getStateList(ds1, cf1);
                    mdaEFSM m = new mdaEFSM(ls, ls[0], ds1);
                    Driver1 d1 = new Driver1();
                    GasPump_1 gp1 = new GasPump_1(ds1, cf1, m);
                    d1.run(gp1);
                }

                // Run Test for Gas Pump 2
                case "2": { 
                    DS2 ds2 = new DS2();
                    CF2 cf2 = new CF2(ds2);
                    State [] ls = getStateList(ds2, cf2);
                    mdaEFSM m = new mdaEFSM(ls, ls[0], ds2);
                    Driver2 d2 = new Driver2();
                    GasPump_2 gp2 = new GasPump_2(ds2, cf2, m);
                    d2.run(gp2);
                }

                default : {
                    System.out.println("Usage: Must have exactly one argument. You may enter\n  1: To select gas pump 1\n  2: To select gas pump 2\n\n");
                }
            } // switch
        } // else
    } // main
}