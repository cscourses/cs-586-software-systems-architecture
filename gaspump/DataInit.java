/*
DataInit.java
This file is part of the Strategy pattern.
*/

package compiled;

public class DataInit{

    // Constructor
    public DataInit(){}

    // Prototypes
    public void InitializeData(){}
}