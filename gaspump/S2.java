/*
S2.java
This file is part of the State pattern.
*/

package compiled;

public class S2 extends State{
    // Constructor
    public S2(OP new_op) {
        super(new_op);
        ID = 3;
    }
    // Actions
    public void Approved(){
        op.DisplayMenu();
    }
    public void Reject(){
        op.RejectMsg();
    }
}
