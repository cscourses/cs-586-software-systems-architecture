/*
SetPrice1.java
This file is part of the Strategy pattern.
*/

// g=1: regular
// g=2: super
// g=3: premium
// g=4: diesel

package compiled;

public class SetPrice1 extends SetPrice{
    // Variables
    DS1 d;

    // Constructor
    public SetPrice1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void SetPrice(){
        // If Regular, set price to Rprice
        if (d.getGasType() == 1){
            d.setPrice(d.getRprice());
        }
        // If Diesel, set price to Dprice
        else if (d.getGasType() == 4){
            d.setPrice(d.getDprice());
        }
    }
}