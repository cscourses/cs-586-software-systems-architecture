/*
CF2.java
This file is part of the Abstract Factory pattern.
*/

package compiled;

public class CF2 extends AbstractFactory {

    // Variables
    DS2 d2;

    // Constructor
    public CF2(DS2 new_d) {
        this.d2 = new_d;
    }

    // Methods
    public Prices getPrices(){
        Prices2 p = new Prices2(d2);
        return p;
    }
    public Cash getCash(){
        Cash2 p = new Cash2(d2);
        return p;
    }
    public InitVals getInitVals(){
        InitVals2 p = new InitVals2(d2);
        return p;
    }
    public Menu getMenu(){
        Menu2 p = new Menu2(d2);
        return p;
    }
    public PIN getPIN(){
        PIN2 p = new PIN2(d2);
        return p;
    }
    public SetPrice getSetPrice(){
        SetPrice2 p = new SetPrice2(d2);
        return p;
    }
    public Pump getPump(){
        Pump2 p = new Pump2(d2);
        return p;
    }
    public DataInit getDataInit(){
        DataInit2 p = new DataInit2(d2);
        return p;
    }
    public Print getPrint(){
        Print2 p = new Print2(d2);
        return p;
    }
    public Change getChange(){
        Change2 p = new Change2(d2);
        return p;
    }
}