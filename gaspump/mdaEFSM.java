/*
mdaEFSM.java

This file serves as the main class for the state pattern, which is linked to the State and account data.
*/

package compiled;

public class mdaEFSM {

    // Variables
    int state;
    State s;
    State [] ls;
    DataStore d;

    // Constructor
    public mdaEFSM(State [] new_ls, State new_s, DataStore new_d){
        this.state = -1;
        this.ls = new_ls;
        this.s = new_s;
        this.d = new_d;
    }

    // Methods
    public void Activate(){
        state = s.getID();
        if (state == 0){
            s.Activate(); // State stores prices from temp data structure
            s = ls[1]; // Change state
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Start(){
        state = s.getID();
        if (state == 1){
            s.Start();
            s = ls[2];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void PayType(){
        state = s.getID();
        if (state == 2){
            s.PayType(d.getTempPT()); 

            // t=3: debit
            // t=2: cash
            // t=1: credit
            
            if (d.getTempPT() == 1){
                s = ls[3];
            }
            else if (d.getTempPT() == 2){
                s = ls[4];
            }
            else if (d.getTempPT() == 3){
                s = ls[8];
            }
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Reject(){
        state = s.getID();
        if (state == 3){
            s.Reject(); // Display reject message
            s = ls[1];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Approved(){
        state = s.getID();
        if (state == 3){
            s.Approved(); // Display the menu
            s = ls[4];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Cancel(){
        state = s.getID();
        if (state == 4){
            s.Cancel(); // Display CancelMsg
            s = ls[1];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void CorrectPin(){
        state = s.getID();
        if (state == 8){
            s.CorrectPin(); // Display the menu
            s = ls[4];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void IncorrectPin(){
        state = s.getID();
        if (state == 8){
            s.IncorrectPin(); // Display WrongPinMsg
            s = ls[1];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void SelectGas(){
        state = s.getID();
        if (state == 4){
            s.SelectGas(); // Based on gas type and M value, do SetPrice()
            // NO CHANGE OF STATE
            // g=1: regular
            // g=2: super
            // g=3: premium
            // g=4: diesel
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Continue(){
        state = s.getID();
        if (state == 4){
            s.Continue(); // does nothing
            s = ls[5];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void StartPump(){
        state = s.getID();
        if (state == 5){
            s.StartPump(); // SetInitialValues, and display ReadyMsg
            s = ls[6];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Pump(){
        state = s.getID();
        if (state == 6){
            s.Pump(); // PumpGasUnit, and display GasPumpedMsg
            // No state change
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void StopPump(){
        state = s.getID();
        if (state == 6){
            s.StopPump(); // Display StopMsg
            s = ls[7];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void Receipt(){
        state = s.getID();
        if (state == 7){
            s.Receipt(); // PrintReceipt and ReturnCash
            s = ls[1];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
    public void NoReceipt(){
        state = s.getID();
        if (state == 7){
            s.NoReceipt(); // ReturnCash
            s = ls[1];
        }
        else System.out.println("Operation invalid in current state " + state);
    }
}