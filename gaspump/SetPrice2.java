/*
SetPrice2.java
This file is part of the Strategy pattern.
*/

// g=1: regular
// g=2: super
// g=3: premium
// g=4: diesel

// t=1: CREDIT
// t=2: CASH
// t=3: DEBIT

package compiled;

public class SetPrice2 extends SetPrice{
    // Variables
    DS2 d;

    // Constructor
    public SetPrice2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void SetPrice(){
        // If Regular, set price to Rprice
        if (d.getGasType() == 1){
            d.setPrice(d.getRprice());
        }
        // If Super, set price to Sprice
        else if (d.getGasType() == 2){
            d.setPrice(d.getSprice());
        }
        // If Premium, set price to Pprice
        else if (d.getGasType() == 3){
            d.setPrice(d.getPprice());
        }
        // Finally, if PayType is Credit, increase price by 1.1 x
        if (d.getPayType() == 1){
            d.setPrice(d.getPrice() * (float)1.1);
        }
    }
}