/*
GasPump_1.java

This file provides the client end method calls to the DataStore, AbstractFactory, and mdaEFSM classes for GasPump_1.
*/

package compiled;

public class GasPump_2 {
    // Variables
    DS2 d;
    CF2 c;
    mdaEFSM m;

    // Constructor
    public GasPump_2(DS2 new_d, CF2 new_c, mdaEFSM new_m) {
        this.d = new_d;
        this.c = new_c;
        this.m = new_m;
    }

    // Methods
    public void Activate(int a, int b, int c){
        if (a>0 && b>0 && c>0) {
            d.setTempA(a);
            d.setTempB(b); 
            d.setTempC(c); 
            m.Activate();
            m.Start();
        }
    }
    public void PayCash(float c){
        if (c>0){
            d.setTempCash(c);
            d.setTempPT(2);
            m.PayType();
        }
    }
    public void PayCredit(){
        d.setTempPT(1);
        m.PayType();
    }
    public void Reject(){
        m.Reject();
        m.Start();
    }
    public void Approved(){
        m.Approved();
    }
    public void Cancel(){
        m.Cancel();
        m.Start();
    }
    public void Super(){
        d.setTempGT(2);
        m.SelectGas();
        m.Continue();
    }
    public void Premium(){
        d.setTempGT(3);
        m.SelectGas();
        m.Continue();
    }
    public void Regular(){
        d.setTempGT(1);
        m.SelectGas();
        m.Continue();
    }
    public void StartPump(){
        m.StartPump();
    }
    public void PumpLiter(){
        // Stop the pump if PayType is cash and not enough is left for another unit
        if ((d.getPayType() == 2) && (d.getCash() < d.getPrice()*(d.getLit() + 1))){
            m.StopPump();
        }
        else{
            m.Pump();
        }
    }
    public void Stop(){
        m.StopPump();
    }
    public void Receipt(){
        m.Receipt();
        m.Start();
    }
    public void NoReceipt(){
        m.NoReceipt();
        m.Start();
    }
}