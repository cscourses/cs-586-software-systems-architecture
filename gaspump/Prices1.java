/*
Prices1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Prices1 extends Prices{
    // Variables
    DS1 d;

    // Constructor
    public Prices1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void StorePrices(){
        d.setRprice(d.getTempA());
        d.setDprice(d.getTempB());
    }
}