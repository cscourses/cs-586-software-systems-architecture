/*
DataInit1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class DataInit1 extends DataInit{
    // Variables
    DS1 d;

    // Constructor
    public DataInit1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void InitializeData(){
        d.setPrice(0);
    }
}