/*
State.java
This file is part of the State pattern.
It contains only abstract versions of methods in states Start and S0-7, and the get method for state ID.
*/

package compiled;

public class State {

    // Variables
    int ID;
    OP op;

    // Constructor
    public State(OP new_op) {
        this.op = new_op;
    }

    // Get method for int ID
    public int getID(){
        return this.ID;
    }

    // Prototypes
    public void Activate(){}
    public void Start(){}
    public void PayType(int pt){}
    public void Reject(){}
    public void Approved(){}
    public void Cancel(){}
    public void CorrectPin(){}
    public void IncorrectPin(){}
    public void SelectGas(){}
    public void Continue(){}
    public void StartPump(){}
    public void Pump(){ }
    public void StopPump(){}
    public void Receipt(){}
    public void NoReceipt(){}
}
