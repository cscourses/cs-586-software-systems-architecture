/*
DataInit2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class DataInit2 extends DataInit{
    // Variables
    DS2 d;

    // Constructor
    public DataInit2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void InitializeData(){
        d.setPrice(0);
        d.setCash(0);
    }
}