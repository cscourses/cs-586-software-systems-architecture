/*
S4.java
This file is part of the State pattern.
*/

package compiled;

public class S4 extends State{
    // Constructor
    public S4(OP new_op) {
        super(new_op);
        ID = 5;
    }
    // Actions
    public void StartPump(){
        op.SetInitialValues();
        op.ReadyMsg();
    }
}
