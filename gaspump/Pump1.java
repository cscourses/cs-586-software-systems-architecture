/*
Pump1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Pump1 extends Pump{
    // Variables
    DS1 d;

    // Constructor
    public Pump1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void PumpGasUnit(){
        d.setGal(d.getGal() + 1);
        d.setTotal(d.getPrice() * d.getGal());
        System.out.println(d.getGal() + " Gallons / $" + d.getTotal());
    }
}