/*
Pump2.java
This file is part of the Strategy pattern.
*/

// t=1: CREDIT
// t=2: CASH
// t=3: DEBIT

package compiled;

public class Pump2 extends Pump{
    // Variables
    DS2 d;

    // Constructor
    public Pump2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void PumpGasUnit(){
        // If PayType is credit, simple like GasPump_1
        if (d.getPayType() == 1){
            d.setLit(d.getLit() + 1);
            d.setTotal(d.getPrice() * d.getLit());
            System.out.println(d.getLit() + " Liters / $" + d.getTotal());
        }
        // If PayType is cash, we can only get here if cash >= price*(L+1) so this condition is moot
        // Therefore, if PayType is cash we know that we can proceed
        else if (d.getPayType() == 2){
            d.setLit(d.getLit() + 1);
            d.setTotal(d.getPrice() * d.getLit());
            System.out.println(d.getLit() + " Liters");
        }
        
    }
}