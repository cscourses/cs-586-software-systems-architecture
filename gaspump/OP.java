/*
OP.java

This file serves as the Device class for the abstract factory and strategy patterns.
*/

package compiled;

public class OP {
    
    // Variables
    DataStore d;
    AbstractFactory af;
    Prices prices;
    Cash cash;
    InitVals i;
    Menu m;
    PIN pin;
    SetPrice s;
    Pump pump;
    DataInit dataInit;
    Print print;
    Change change;

    // Constructor
    public OP(DataStore new_d, AbstractFactory new_af) {
        this.d = new_d;
        this.af = new_af;
    }

    // Message Methods
    public void PayMsg(){
        System.out.println("Please select a form of payment from the menu.");
    }
    public void WrongPinMsg(){
        System.out.println("PIN is incorrect; you have been returned to the original activated state.");
    }
    public void EnterPinMsg(){
        System.out.println("True PIN has been acquired, please select (7) PIN to verify.");
    }
    public void RejectMsg(){
        System.out.println("This credit card has been rejected; you have been returned to the original activated state.");
    }
    public void CancelMsg(){
        System.out.println("Session cancelled; you have been returned to the original activated state.");
    }
    public void ReadyMsg(){
        System.out.println("Ready to pump.");
    }
    public void GasPumpedMsg(){
        System.out.println("Pumped one unit of gas.");
    }
    public void StopMsg(){
        System.out.println("Pumping stopped.");
    }

    // Non-specific functional methods
    public void StorePT(){
        d.setPayType(d.getTempPT());
    }
    public void StoreGT(){
        d.setGasType(d.getTempGT());
    }

    // Specific functional methods (require strategy/abstract factory pattern)
    public void StorePrices(){
        this.prices = af.getPrices();
        prices.StorePrices();
    }
    public void SetInitialValues(){
        this.i = af.getInitVals();
        i.SetInitialValues();
    }
    public void StoreCash(){
        this.cash = af.getCash();
        cash.StoreCash();
    }
    public void DisplayMenu(){
        this.m = af.getMenu();
        m.DisplayMenu();
    }
    public void StorePin(){
        this.pin = af.getPIN();
        pin.StorePin();
    }
    public void SetPrice(){
        this.s = af.getSetPrice();
        s.SetPrice();
    }
    public void InitializeData(){
        this.dataInit = af.getDataInit();
        dataInit.InitializeData();
    }
    public void PumpGasUnit(){
        this.pump = af.getPump();
        pump.PumpGasUnit();
    }
    public void PrintReceipt(){
        this.print = af.getPrint();
        print.PrintReceipt();
    }
    public void ReturnCash(){
        this.change = af.getChange();
        change.ReturnCash();
    }
}