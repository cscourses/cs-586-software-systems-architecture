/*
InitVals2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class InitVals2 extends InitVals{
    // Variables
    DS2 d;

    // Constructor
    public InitVals2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void SetInitialValues(){
        d.setTotal((float)0.0);
        d.setLit(0);
    }
}