/*
Change1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Change1 extends Change{
    // Variables
    DS1 d;

    // Constructor
    public Change1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods: Empty because GasPump_1 does not allow cash transactions
    public void ReturnCash(){}
}