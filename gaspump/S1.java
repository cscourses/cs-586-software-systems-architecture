/*
S1.java
This file is part of the State pattern.
*/

package compiled;

public class S1 extends State{
    // Constructor
    public S1(OP new_op) {
        super(new_op);
        ID = 2;
    }
    // Action
    public void PayType(int pt){ 
        op.StorePT();
        if(pt == 2){
            op.DisplayMenu();
            op.StoreCash();
        }
        if(pt == 3){
            op.StorePin();
            op.EnterPinMsg();
        }
    }
}
