/*
S7.java
This file is part of the State pattern.
*/

package compiled;

public class S7 extends State{
    // Constructor
    public S7(OP new_op) {
        super(new_op);
        ID = 8;
    }
    // Actions
    public void IncorrectPin(){
        op.WrongPinMsg();
    }
    public void CorrectPin(){
        op.DisplayMenu();
    }
}