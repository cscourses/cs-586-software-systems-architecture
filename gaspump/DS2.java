/*
DS2.java

This file gives a constructor and set/get methods for all unique DS2 variables.
*/

package compiled;

public class DS2 extends DataStore {

    // Variables
    int temp_a;
    int temp_b;
    int temp_c;
    float temp_cash;
    float cash;
    float Sprice;
    float Rprice;
    float Pprice;
    int L;

    // Constructor
    public DS2() {
        super();
        this.temp_a = -1;
        this.temp_b = -1;
        this.temp_c = -1;
        this.temp_cash = -1;
        this.cash = -1;
        this.Sprice = -1;
        this.Rprice = -1;
        this.Pprice = -1;
        this.L = -1;
        this.price = -1;
        this.total = -1;
    }

    // Get/set for global vars
    public int getTempA(){
        return this.temp_a;
    }
    public void setTempA(int a){
        this.temp_a = a;
    }
    public int getTempB(){
        return this.temp_b;
    }
    public void setTempB(int b){
        this.temp_b = b;
    }
    public int getTempC(){
        return this.temp_c;
    }
    public void setTempC(int c){
        this.temp_c = c;
    }
    public float getTempCash(){
        return this.temp_cash;
    }
    public void setTempCash(float c){
        this.temp_cash = c;
    }
    public float getCash(){
        return this.cash;
    }
    public void setCash(float c){
        this.cash = c;
    }
    public float getRprice(){
        return this.Rprice;
    }
    public void setRprice(float r){
        this.Rprice = r;
    }
    public float getSprice(){
        return this.Sprice;
    }
    public void setSprice(float s){
        this.Sprice = s;
    }
    public float getPprice(){
        return this.Pprice;
    }
    public void setPprice(float p){
        this.Pprice = p;
    }
    public int getLit(){
        return this.L;
    }
    public void setLit(int lit){
        this.L = lit;
    }
}