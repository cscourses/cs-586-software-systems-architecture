/*
S6.java
This file is part of the State pattern.
*/

package compiled;

public class S6 extends State{
    // Constructor
    public S6(OP new_op) {
        super(new_op);
        ID = 7;
    }
    // Actions
    public void Receipt(){
        op.PrintReceipt();
        op.ReturnCash();
    }
    public void NoReceipt(){
        op.ReturnCash();
    }
}
