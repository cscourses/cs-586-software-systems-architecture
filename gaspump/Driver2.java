/*
Driver2.java

This file allows us to test GasPump_2.
*/

package compiled;
import java.util.Scanner;
import java.lang.*;

public class Driver2{
    // Constructor
    public Driver2() {}

    // Run method
    public void run(GasPump_2 gp2){
        int a, b, c;
        int cash;
        char ch;

        //Menu
        System.out.println("\t\t\t\t\tGasPump-2");
        System.out.println("\t\tMENU of Operations");
        System.out.println("\t\t0. Activate(int, int, int)");
        System.out.println("\t\t1. PayCash(int)");
        System.out.println("\t\t2. PayCredit()");
        System.out.println("\t\t3. Approved()");
        System.out.println("\t\t4. Reject()");
        System.out.println("\t\t5. Cancel()");
        System.out.println("\t\t6. Premium()");
        System.out.println("\t\t7. Regular()");
        System.out.println("\t\t8. Super()");
        System.out.println("\t\t9. StartPump()");
        System.out.println("\t\ta. PumpLiter()");
        System.out.println("\t\tb. Stop()");
        System.out.println("\t\tc. Receipt()");
        System.out.println("\t\td. NoReceipt()");
        System.out.println("\t\tq. Quit the program");
        System.out.println("\t\tPlease make a note of these operations");
        System.out.println("\t\tGasPump-2 Execution");
        System.out.flush();

        Scanner reader=new Scanner(System.in);
        ch = '1';
        while (ch !='q') {
            System.out.println("\n0-Activate, 1-PayCash, 2-PayCredit, 3-Approved, 4-Reject, 5-Cancel, 6-Premium,");
            System.out.println("7-Regular, 8-Super, 9-StartPump, a-PumpLiter, b-Stop, c-Receipt, d-NoReceipt, q-quit");
            System.out.print("\nSelect an Operation: ");
            ch = (char) reader.next().charAt(0);
            switch (ch) {
                case '0': { //Activate()
                    System.out.println(" Running Activate(int a, int b, int c)");
                    System.out.print(" Enter value a: ");
                    a = reader.nextInt();
                    System.out.print(" Enter value b: ");
                    b = reader.nextInt();
                    System.out.print(" Enter value c: ");
                    c = reader.nextInt();
                    gp2.Activate(a,b,c);
                    break;
                }
                case '1': { //PayCash
                    System.out.println(" Running PayCash(int c)");
                    System.out.print(" Enter value c: ");
                    cash = reader.nextInt();
                    gp2.PayCash(cash);
                    break;
                }
                case '2': { //PayCredit
                    System.out.println(" Running PayCredit()");
                    gp2.PayCredit();
                    break;
                }
                case '3': { //Approved
                    System.out.println(" Running Approved()");
                    gp2.Approved();
                    break;
                }
                case '4': { //Reject
                    System.out.println(" Running Reject()");
                    gp2.Reject();
                    break;
                }
                case '5': { //Cancel
                    System.out.println(" Running Cancel()");
                    gp2.Cancel();
                    break;
                }
                case '6': { //Premium
                    System.out.println(" Running Premium()");
                    gp2.Premium();
                    break;
                }
                case '7': { //Regular
                    System.out.println(" Running Regular()");
                    gp2.Regular();
                    break;
                }
                case '8': { //Super
                    System.out.println(" Running Super()");
                    gp2.Super();
                    break;
                }
                case '9': { //StartPump
                    System.out.println(" Running StartPump()");
                    gp2.StartPump();
                    break;
                }
                case 'a': { //PumpLiter
                    System.out.println(" Running PumpLiter()");
                    gp2.PumpLiter();
                    break;
                }
                case 'b': { //Stop
                    System.out.println(" Running Stop()");
                    gp2.Stop();
                    break;
                }
                case 'c': { //Receipt
                    System.out.println(" Running Receipt()");
                    gp2.Receipt();
                    break;
                }
                case 'd': { //NoReceipt
                    System.out.println(" Running NoReceipt()");
                    gp2.NoReceipt();
                    break;
                }
                case 'q': {
                    reader.close();
                    System.exit(0);
                }
                default: {
                    System.out.println("Please enter an operation from the list.");
                }
            } // end Input switch
        } // endwhile
        reader.close();
    } // end run()
} // end Driver2