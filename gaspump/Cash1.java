/*
Cash1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Cash1 extends Cash{
    // Variables
    DS1 d;

    // Constructor
    public Cash1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods: nothing in this one because GasPump_1 does not accept cash
    public void StoreCash(){}
}