/*
S0.java
This file is part of the State pattern.
*/

package compiled;

public class S0 extends State{
    // Constructor
    public S0(OP new_op) {
        super(new_op);
        ID = 1;
    }
    // Action
    public void Start(){
        op.PayMsg();
        op.InitializeData();
    }
}
