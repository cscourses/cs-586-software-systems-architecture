/*
DataStore.java

This file gives a constructor and set/get methods for common variables between DS1 and DS2.
Other methods depend on the subclass.
*/

package compiled;

public class DataStore {

    // Variables
    float price;
    float total;
    int tempGT;
    int tempPT;
    int gasType;
    int payType;

    // Constructor
    public DataStore() {
        this.price = -1;
        this.total = -1;
        this.tempGT = -1;
        this.tempPT = -1;
        this.gasType = -1;
        this.payType = -1;
    }

    // Get/set for global vars
    public float getPrice(){
        return this.price;
    }
    public void setPrice(float p){
        this.price = p;
    }
    public float getTotal(){
        return this.total;
    }
    public void setTotal(float t){
        this.total = t;
    }
    public int getTempGT(){
        return this.tempGT;
    }
    public void setTempGT(int g){
        this.tempGT = g;
    }
    public int getGasType(){
        return this.gasType;
    }
    public void setGasType(int g){
        this.gasType = g;
    }
    public int getTempPT(){
        return this.tempPT;
    }
    public void setTempPT(int p){
        this.tempPT = p;
    }
    public int getPayType(){
        return this.payType;
    }
    public void setPayType(int p){
        this.payType = p;
    }
}