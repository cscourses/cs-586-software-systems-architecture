/*
InitVals1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class InitVals1 extends InitVals{
    // Variables
    DS1 d;

    // Constructor
    public InitVals1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void SetInitialValues(){
        d.setTotal((float)0.0);
        d.setGal(0);
    }
}