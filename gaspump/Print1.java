/*
Print1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Print1 extends Print{
    // Variables
    DS1 d;

    // Constructor
    public Print1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void PrintReceipt(){
        System.out.println("\n\t\t\t\t######## RECEIPT ########");
        System.out.println("\t\t\t\t" + d.getGal() + " Gallons");
        System.out.println("\t\t\t\t$" + d.getTotal() + "\n");
    }
}