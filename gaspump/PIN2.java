/*
PIN2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class PIN2 extends PIN{
    // Variables
    DS2 d;

    // Constructor
    public PIN2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods: Empty because debit transactions, and therefore PIN calls, are not available on GasPump_2
    public void StorePin(){}
}