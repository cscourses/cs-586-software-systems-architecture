/*
Print2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Print2 extends Print{
    // Variables
    DS2 d;

    // Constructor
    public Print2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void PrintReceipt(){
        System.out.println("\n\t\t\t\t######## RECEIPT ########");
        System.out.println("\t\t\t\t" + d.getLit() + " Liters");
        System.out.println("\t\t\t\t$" + d.getTotal() + "\n");
    }
}