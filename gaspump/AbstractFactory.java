/*
AbstractFactory.java
This file is part of the Abstract Factory pattern.
It contains only abstract versions of methods in CF1 and CF2.
*/

package compiled;

public abstract class AbstractFactory {

    // Constructor
    public AbstractFactory() {}

    // Prototype methods
    public abstract Prices getPrices();
    public abstract Cash getCash();
    public abstract InitVals getInitVals();
    public abstract Menu getMenu();
    public abstract PIN getPIN();
    public abstract SetPrice getSetPrice();
    public abstract Pump getPump();
    public abstract DataInit getDataInit();
    public abstract Print getPrint();
    public abstract Change getChange();
}