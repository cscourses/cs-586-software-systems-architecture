/*
Menu1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Menu1 extends Menu{
    // Variables
    DS1 d;

    // Constructor
    public Menu1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void DisplayMenu(){
        System.out.println("\n#### MAIN MENU ####\nPlease select one: \n5: Cancel Transaction\n8: Diesel\n9: Regular\nOnce a gas type is chosen, select (a) to start the pump");
    }
}