/*
Change2.java
This file is part of the Strategy pattern.
*/

// t=1: CREDIT
// t=2: CASH
// t=3: DEBIT

package compiled;

public class Change2 extends Change{
    // Variables
    DS2 d;

    // Constructor
    public Change2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void ReturnCash(){
        // If user paid with cash
        if (d.getPayType() == 2){
            float change = d.getCash() - d.getTotal();
            System.out.println("\t\t\t\tCash Returned: $" + change);
        }
    }
}