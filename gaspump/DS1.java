/*
DS1.java

This file gives a constructor and set/get methods for all unique DS1 variables.
*/


package compiled;

public class DS1 extends DataStore{

    // Variables
    float temp_a;
    float temp_b;
    String temp_pin;
    String pin;
    float Rprice;
    float Dprice;
    int G;

    // Constructor
    public DS1() {
        super();
        this.temp_a = -1;
        this.temp_b = -1;
        this.temp_pin = "-1";
        this.pin = "-1";
        this.Rprice = -1;
        this.Dprice = -1;
        this.G = -1;
        this.price = -1;
        this.total = -1;
    }

    // Get/set for global vars
    public float getTempA(){
        return this.temp_a;
    }
    public void setTempA(float a){
        this.temp_a = a;
    }
    public float getTempB(){
        return this.temp_b;
    }
    public void setTempB(float b){
        this.temp_b = b;
    }
    public String getTempPin(){
        return this.temp_pin;
    }
    public void setTempPin(String p){
        this.temp_pin = p;
    }
    public String getPin(){
        return this.pin;
    }
    public void setPin(String p){
        this.pin = p;
    }
    public float getRprice(){
        return this.Rprice;
    }
    public void setRprice(float r){
        this.Rprice = r;
    }
    public float getDprice(){
        return this.Dprice;
    }
    public void setDprice(float d){
        this.Dprice = d;
    }
    public int getGal(){
        return this.G;
    }
    public void setGal(int g){
        this.G = g;
    }
}