/*
CF1.java
This file is part of the Abstract Factory pattern.
*/

package compiled;

public class CF1 extends AbstractFactory {

    // Variables
    DS1 d1;

    // Constructor
    public CF1(DS1 new_d) {
        this.d1 = new_d;
    }

    // Methods
    public Prices getPrices(){
        Prices1 p = new Prices1(d1);
        return p;
    }
    public Cash getCash(){
        Cash1 p = new Cash1(d1);
        return p;
    }
    public InitVals getInitVals(){
        InitVals1 p = new InitVals1(d1);
        return p;
    }
    public Menu getMenu(){
        Menu1 p = new Menu1(d1);
        return p;
    }
    public PIN getPIN(){
        PIN1 p = new PIN1(d1);
        return p;
    }
    public SetPrice getSetPrice(){
        SetPrice1 p = new SetPrice1(d1);
        return p;
    }
    public Pump getPump(){
        Pump1 p = new Pump1(d1);
        return p;
    }
    public DataInit getDataInit(){
        DataInit1 p = new DataInit1(d1);
        return p;
    }
    public Print getPrint(){
        Print1 p = new Print1(d1);
        return p;
    }
    public Change getChange(){
        Change1 p = new Change1(d1);
        return p;
    }
}