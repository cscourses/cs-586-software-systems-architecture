/*
Menu2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Menu2 extends Menu{
    // Variables
    DS2 d;

    // Constructor
    public Menu2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void DisplayMenu(){
        System.out.println("\n#### MAIN MENU ####\nPlease select one: \n5: Cancel Transaction\n6: Premium\n7: Regular\n8: Super\nOnce a gas type is chosen, select (9) to start the pump");
    }
}