/*
S3.java
This file is part of the State pattern.
*/

package compiled;

public class S3 extends State{
    // Constructor
    public S3(OP new_op) {
        super(new_op);
        ID = 4;
    }
    // Actions
    public void Cancel(){
        op.CancelMsg();
    }
    public void SelectGas(){
        op.StoreGT();
        op.SetPrice();
    }
    public void Continue(){
        // No functional action taken
    }
}
