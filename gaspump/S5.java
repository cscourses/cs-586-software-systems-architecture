/*
S5.java
This file is part of the State pattern.
*/

package compiled;

public class S5 extends State{
    // Constructor
    public S5(OP new_op) {
        super(new_op);
        ID = 6;
    }
    // Actions
    public void Pump(){
        op.PumpGasUnit();
        op.GasPumpedMsg();
    }
    public void StopPump(){
        op.StopMsg();
    }
}
