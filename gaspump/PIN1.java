/*
PIN1.java
This file is part of the Strategy pattern.
*/

package compiled;

public class PIN1 extends PIN{
    // Variables
    DS1 d;

    // Constructor
    public PIN1(DS1 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void StorePin(){
        d.setPin(d.getTempPin());
    }
}