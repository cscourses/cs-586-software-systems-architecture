/*
Start.java
This file is part of the State pattern.
*/

package compiled;

public class Start extends State{
    // Constructor
    public Start(OP new_op) {
        super(new_op);
        ID = 0;
    }
    // Action
    public void Activate(){
        op.StorePrices();
    }
}
