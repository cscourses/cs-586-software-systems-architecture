/*
GasPump_1.java

This file provides the client end method calls to the DataStore, AbstractFactory, and mdaEFSM classes for GasPump_1.
*/

package compiled;

public class GasPump_1 {
    // Variables
    DS1 d;
    CF1 c;
    mdaEFSM m;

    // Constructor
    public GasPump_1(DS1 new_d, CF1 new_c, mdaEFSM new_m) {
        this.d = new_d;
        this.c = new_c;
        this.m = new_m;
    }

    // Methods
    public void Activate(float a, float b){
        if (a>0 && b>0) {
            d.setTempA(a); 
            d.setTempB(b); 
            m.Activate();
        }
    }
    public void Start(){
        m.Start();
    }
    public void PayCredit(){
        d.setTempPT(1);
        m.PayType();
    }
    public void Reject(){
        m.Reject();
    }
    public void Approved(){
        m.Approved();
    }
    public void Cancel(){
        m.Cancel();
    }
    public void PayDebit(String p){
        d.setTempPin(p);
        d.setTempPT(3);
        m.PayType();
    }
    public void Pin(String x){
        if (d.getPin().equals(x)) {
            m.CorrectPin();
        }
        else {
            m.IncorrectPin();
        }
    }
    public void Diesel(){
        d.setTempGT(4);
        m.SelectGas();
    }
    public void Regular(){
        d.setTempGT(1);
        m.SelectGas();
    }
    public void StartPump(){
        if (d.getPrice() > 0){
            m.Continue();
            m.StartPump();
        }
    }
    public void PumpGallon(){
        m.Pump();
    }
    public void StopPump(){
        m.StopPump();
        m.Receipt();
    }
    public void FullTank(){
        m.StopPump();
        m.Receipt();
    }
}