/*
Driver1.java

This file allows us to test GasPump_1.
*/

package compiled;
import java.util.Scanner;
import java.lang.*;

public class Driver1{
    // Constructor
    public Driver1() {}

    // Run method
    public void run(GasPump_1 gp1){
        float a, b;
        String true_PIN, input_PIN;
        char ch;

        //Menu
        System.out.println("\t\t\t\t\tGasPump-1");
        System.out.println("\t\tMENU of Operations");
        System.out.println("\t\t0. Activate(float, float)");
        System.out.println("\t\t1. Start()");
        System.out.println("\t\t2. PayCredit()");
        System.out.println("\t\t3. Approved()");
        System.out.println("\t\t4. Reject()");
        System.out.println("\t\t5. Cancel()");
        System.out.println("\t\t6. PayDebit()");
        System.out.println("\t\t7. Pin()"); 
        System.out.println("\t\t8. Diesel()");
        System.out.println("\t\t9. Regular()");
        System.out.println("\t\ta. StartPump()");
        System.out.println("\t\tb. PumpGallon()");
        System.out.println("\t\tc. StopPump()"); 
        System.out.println("\t\td. FullTank()"); 
        System.out.println("\t\tq. Quit the program");
        System.out.println("\t\tPlease make a note of these operations");
        System.out.println("\t\tGasPump-1 Execution");
        System.out.flush();

        Scanner reader = new Scanner(System.in);
        ch = '1';
        while (ch !='q') {
            System.out.println("\n0-Activate, 1-Start, 2-PayCredit, 3-Approved, 4-Reject, 5-Cancel, 6-PayDebit,");
            System.out.println("7-Pin, 8-Diesel, 9-Regular, a-StartPump, b-PumpGallon, c-StopPump, d-FullTank, q-quit");
            System.out.print("\nSelect an Operation: ");
            ch = (char) reader.next().charAt(0);
            switch (ch) {
                case '0': { //Activate()
                    System.out.println(" Running Activate(float a, float b)");
                    System.out.print(" Enter value a: ");
                    a = reader.nextFloat();
                    System.out.print(" Enter value b: ");
                    b = reader.nextFloat();
                    gp1.Activate(a,b);
                    break;
                }
                case '1': { //Start()
                    System.out.println(" Running Start()");
                    gp1.Start();
                    break;
                }
                case '2': { //PayCredit
                    System.out.println(" Running PayCredit()");
                    gp1.PayCredit();
                    break;
                }
                case '3': { //Approved
                    System.out.println(" Running Approved()");
                    gp1.Approved();
                    break;
                }
                case '4': { //Reject
                    System.out.println(" Running Reject()");
                    gp1.Reject();
                    break;
                }
                case '5': { //Cancel
                    System.out.println(" Running Cancel()");
                    gp1.Cancel();
                    break;
                }
                case '6': { //PayDebit
                    System.out.println(" Running PayDebit(String p)");
                    System.out.print(" Enter true pin: ");
                    true_PIN = reader.next();
                    gp1.PayDebit(true_PIN);
                    break;
                }
                case '7': { //Pin
                    System.out.println(" Running Pin(String p)");
                    System.out.print(" Enter pin to compare: ");
                    input_PIN = reader.next();
                    gp1.Pin(input_PIN);
                    break;
                }
                case '8': { //Diesel
                    System.out.println(" Running Diesel()");
                    gp1.Diesel();
                    break;
                }
                case '9': { //Regular
                    System.out.println(" Running Regular()");
                    gp1.Regular();
                    break;
                }
                case 'a': { //StartPump
                    System.out.println(" Running StartPump()");
                    gp1.StartPump();
                    break;
                }
                case 'b': { //PumpGallon
                    System.out.println(" Running PumpGallon()");
                    gp1.PumpGallon();
                    break;
                }
                case 'c': { //StopPump
                    System.out.println(" Running StopPump()");
                    gp1.StopPump();
                    break;
                }
                case 'd': { //FullTank
                    System.out.println(" Running FullTank()");
                    gp1.FullTank();
                    break;
                }
                case 'q': {
                    reader.close();
                    System.exit(0);
                }
                default: {
                    System.out.println("Please enter an operation from the list.");
                }
            } // end Input switch
        } // endwhile
        reader.close();
    } // end run()
} // end Driver1