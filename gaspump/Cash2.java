/*
Cash2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Cash2 extends Cash{
    // Variables
    DS2 d;

    // Constructor
    public Cash2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void StoreCash(){
        d.setCash(d.getTempCash());
    }
}