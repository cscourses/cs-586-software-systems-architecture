/*
Prices2.java
This file is part of the Strategy pattern.
*/

package compiled;

public class Prices2 extends Prices{
    // Variables
    DS2 d;

    // Constructor
    public Prices2(DS2 new_d){
        super();
        this.d = new_d;
    }

    // Methods
    public void StorePrices(){
        d.setSprice(d.getTempA());
        d.setRprice(d.getTempB());
        d.setPprice(d.getTempC());
    }
}